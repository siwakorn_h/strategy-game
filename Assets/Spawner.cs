﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject Obstacle1;
    public GameObject Obstacle2;
    public GameObject Obstacle3;

    public float spawntime;
    public float starttime;
    public float decrease;
    public float mintime = 0.65f;

    public float SpawnChance = 49;
    public float randomizor;
    public float randomizorMin = 0;
    public float randomizorMax = 99;
    public float max = 99;

    public Transform JumperSpawn;

    private void Update()
    {
        if (randomizorMin > 0)
        {
            randomizorMin -= 0.5f * Time.deltaTime;
        }
        else
        {
            randomizorMin = 0;
        }

        if (randomizorMax < max)
        {
            randomizorMax += 0.5f * Time.deltaTime;
        }
        else
        {
            randomizorMax = max;
        }

        if (spawntime <= 0)
        {
            randomizor = Random.Range(randomizorMin, randomizorMax);


            if (randomizor <= 99 && randomizor >= 50)
            {
                Instantiate(Obstacle1, transform.position, Quaternion.identity);
            }
            if (randomizor <= 49 && randomizor >= 30)
            {
                Instantiate(Obstacle2, transform.position, Quaternion.identity);
            }
            if (randomizor <= 29 && randomizor >= 25)
            {
                Instantiate(Obstacle3, transform.position, Quaternion.identity);
            }
            spawntime = starttime;

            if (spawntime > mintime)
            {
                starttime -= decrease;
                if (starttime < mintime)
                {
                    starttime = mintime;
                }
            }

        }

        else
        {
            spawntime -= Time.deltaTime;
        }
    }
}
