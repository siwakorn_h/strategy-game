﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public List<AudioClip> audiolist = new List<AudioClip>();
    public AudioSource audiosource;
    public AudioSource audiomusic;

    public void playsound(string name)
    {
        foreach (AudioClip Clip in audiolist)
        {
            if (Clip.name.Equals(name))
            {
                audiosource.clip = Clip;
                audiosource.PlayOneShot(Clip);
            }


        }


    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
