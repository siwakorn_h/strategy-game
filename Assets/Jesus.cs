﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

//This is the 3rd monster
public class Jesus : MonoBehaviour
{
    public Audio manage;
    public bool loop = true;

    public TextMeshProUGUI amountneed;
    public TextMeshProUGUI rage;
    public GameObject gameover;
    public GameObject redeye;

    public RageBar ragebar;
    public Button button;
    public int fruitwant;
    public int randomizor;
    public Ngor ngor;

    public int maxRage = 50;
    public int currentRage;

    public Image apple;
    public Image grape;
    public Image melon;
    public Image dragon;

    private void UIUpdater()
    {
        amountneed.text = randomizor.ToString();
        rage.text = currentRage.ToString();
    }

    public void Feed()
    {
        if (fruitwant == 3)
        {
            if (ngor.currentmelon >= randomizor)
            {
                ngor.currentmelon -= randomizor;
                ragebar.SetHealth(currentRage += randomizor * 8);

                fruitwant = Random.Range(3, 5);
                if (fruitwant == 3)
                {
                    randomizor = Random.Range(2, 4);
                }
                else if (fruitwant == 4)
                {
                    randomizor = Random.Range(1, 3);
                }
                manage.playsound("Halo");
            }
        }
        else if (fruitwant == 4)
        {
            if (ngor.currentdragon >= randomizor)
            {
                ngor.currentdragon -= randomizor;
                ragebar.SetHealth(currentRage += randomizor * 12);

                if (fruitwant == 3)
                {
                    randomizor = Random.Range(2, 4);
                }
                else if (fruitwant == 4)
                {
                    randomizor = Random.Range(1, 3);
                }
                manage.playsound("Halo");
            }
        }
    }
    void Start()
    {
        currentRage = maxRage;
        ragebar.SetMaxHealth(maxRage);

        fruitwant = 3;
        randomizor = 2;

    }
    void Update()
    {
        UIUpdater();
        if (currentRage <= 0)
        {
            gameover.SetActive(true);
            redeye.SetActive(true);
            if (loop)
            {
                loop = !loop;
                manage.playsound("RedEye");
            }
        }
        if (currentRage > maxRage)
        {
            currentRage = maxRage;
        }

        if (fruitwant == 1)
        {
            apple.enabled = true;
        }
        else
        {
            apple.enabled = false;
        }
        if (fruitwant == 2)
        {
            grape.enabled = true;
        }
        else
        {
            grape.enabled = false;
        }
        if (fruitwant == 3)
        {
            melon.enabled = true;
        }
        else
        {
            melon.enabled = false;
        }
        if (fruitwant == 4)
        {
            dragon.enabled = true;
        }
        else
        {
            dragon.enabled = false;
        }
    }
}
