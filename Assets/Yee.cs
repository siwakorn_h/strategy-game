﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

    //This is the 2nd monster
public class Yee : MonoBehaviour
{
    public Audio manage;
    public GameObject mother;
    public bool loop = true;

    public TextMeshProUGUI amountneed;
    public TextMeshProUGUI rage;
    public GameObject gameover;
    public GameObject redeye;

    public RageBar ragebar;
    public Button button;
    public int fruitwant;
    public int randomizor;
    public Ngor ngor;

    public int maxRage = 20;
    public int currentRage;

    public Image apple;
    public Image grape;
    public Image melon;
    public Image dragon;

    private void UIUpdater()
    {
        amountneed.text = randomizor.ToString();
        rage.text = currentRage.ToString();
    }

    public void Feed()
    {
        if (fruitwant == 2)
        {
            if (ngor.currentgrape >= randomizor)
            {
                ngor.currentgrape -= randomizor;
                ragebar.SetHealth(currentRage += randomizor * 6);

                fruitwant = Random.Range(2, 4);
                if (fruitwant == 1)
                {
                    randomizor = Random.Range(3, 8);
                }
                else if (fruitwant == 2)
                {
                    randomizor = Random.Range(3, 6);
                }
                else if (fruitwant == 3)
                {
                    randomizor = Random.Range(2, 4);
                }
                else if (fruitwant == 4)
                {
                    randomizor = Random.Range(1, 3);
                }
                manage.playsound("Yee");
            }
        }
        else if (fruitwant == 3)
        {
            if (ngor.currentmelon >= randomizor)
            {
                ngor.currentmelon -= randomizor;
                ragebar.SetHealth(currentRage += randomizor * 8);

                fruitwant = Random.Range(2, 4);
                if (fruitwant == 1)
                {
                    randomizor = Random.Range(4, 8);
                }
                else if (fruitwant == 2)
                {
                    randomizor = Random.Range(3, 6);
                }
                else if (fruitwant == 3)
                {
                    randomizor = Random.Range(2, 4);
                }
                else if (fruitwant == 4)
                {
                    randomizor = Random.Range(1, 2);
                }
                manage.playsound("Yee");
            }
        }
    }
        void Start()
    {
        currentRage = maxRage;
        ragebar.SetMaxHealth(maxRage);

        fruitwant = 2;
        randomizor = 3;

    }
    void Update()
    {
        UIUpdater();
        if (currentRage <= 0)
        {
            gameover.SetActive(true);
            redeye.SetActive(true);
            if (loop)
            {
                loop = !loop;
                manage.playsound("RedEye");
            }
        }
        if (currentRage > maxRage)
        {
            currentRage = maxRage;
        }

        if (fruitwant == 1)
        {
            apple.enabled = true;
        }
        else
        {
            apple.enabled = false;
        }
        if (fruitwant == 2)
        {
            grape.enabled = true;
        }
        else
        {
            grape.enabled = false;
        }
        if (fruitwant == 3)
        {
            melon.enabled = true;
        }
        else
        {
            melon.enabled = false;
        }
        if (fruitwant == 4)
        {
            dragon.enabled = true;
        }
        else
        {
            dragon.enabled = false;
        }
    }
}
