﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class Button : MonoBehaviour
{
    public Audio manage;
    public bool loop = false;
    public bool loop2 = false;

    public TextMeshProUGUI applescore;
    public TextMeshProUGUI grapescore;
    public TextMeshProUGUI melonscore;
    public TextMeshProUGUI dragonscore;

    public TextMeshProUGUI test;
    public TextMeshProUGUI score;
    public int turn;

    public RageBar ragebar;
    public RageBar ragebar2;
    public RageBar ragebar3;
    public Ngor ngor;
    public Yee yee;
    public Jesus jesus;

    public GameObject monster2;
    public GameObject monster3;

    private void UIUpdater()
    {
        applescore.text = ngor.currentapple.ToString();
        grapescore.text = ngor.currentgrape.ToString();
        melonscore.text = ngor.currentmelon.ToString();
        dragonscore.text = ngor.currentdragon.ToString();

        score.text = turn.ToString();
        test.text = turn.ToString();
    }

    public void ScoreApple()
    {
        ngor.currentapple++;
        ragebar.SetHealth(ngor.currentRage -= 1);
        ragebar2.SetHealth(yee.currentRage -= 1);
        ragebar3.SetHealth(jesus.currentRage -= 1);
        turn++;
    }

    public void ScoreGrape()
    {
        ngor.currentgrape++;
        ragebar.SetHealth(ngor.currentRage -= 2);
        ragebar2.SetHealth(yee.currentRage -= 2);
        ragebar3.SetHealth(jesus.currentRage -= 2);
        turn++;
    }

    public void ScoreMelon()
    {
        ngor.currentmelon++;
        ragebar.SetHealth(ngor.currentRage -= 3);
        ragebar2.SetHealth(yee.currentRage -= 3);
        ragebar3.SetHealth(jesus.currentRage -= 3);
        turn++;
    }

    public void ScoreDragon()
    {
        ngor.currentdragon++;
        ragebar.SetHealth(ngor.currentRage -= 5);
        ragebar2.SetHealth(yee.currentRage -= 5);
        ragebar3.SetHealth(jesus.currentRage -= 5);
        turn++;
    }

    void Start()
    {
        ngor.mother.transform.localPosition = new Vector2(-148, 18);
    }


    void Update()
    {
        UIUpdater();

        if (turn >= 30)
        {
            monster2.SetActive(true);
            if (loop)
            {
                loop = !loop;
                manage.playsound("Yee");
                ngor.mother.transform.localPosition = new Vector2(-89, -50);
                //manage.audiomusic.Pause();
            }
        }
        else
        {
            monster2.SetActive(false);
            yee.currentRage = yee.maxRage;
        }

        if (turn >= 60)
        {
            monster3.SetActive(true);
            if (loop2)
            {
                loop2 = !loop2;
                manage.playsound("Halo");
                ngor.mother.transform.localPosition = new Vector2(-83.3f, -52.4f);
                yee.mother.transform.localPosition = new Vector2(-1, 9);
                //manage.audiomusic.Pause();
            }
        }
        else
        {
            monster3.SetActive(false);
            jesus.currentRage = jesus.maxRage;
        }


    }
}
