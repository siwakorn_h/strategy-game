﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

    //This is the 1st monster
public class Ngor : MonoBehaviour
{
    public Audio manage;
    public GameObject mother;
    public bool loop = true;

    public TextMeshProUGUI amountneed;
    public TextMeshProUGUI rage;
    public GameObject gameover;
    public GameObject redeye;
    public RageBar ragebar;
    public Button button;

    public int currentapple;
    public int currentgrape;
    public int currentmelon;
    public int currentdragon;

    private int maxRage = 20;
    public int currentRage;

    public Image apple;
    public Image grape;
    public Image melon;
    public Image dragon;

    public int fruitwant;
    public int randomizor;
    //public TextMeshProUGUI foodamount;
    //[SerializeField] private string foodformat;

    private void UIUpdater()
    {
        amountneed.text = randomizor.ToString();
        rage.text = currentRage.ToString();
    }

    public void Feed()
    {
        if (fruitwant == 1)
        {
            if (currentapple >= randomizor)
            {
                currentapple -= randomizor;
                ragebar.SetHealth(currentRage += randomizor * 4);

                fruitwant = Random.Range(1, 3);
                if (fruitwant == 1)
                {
                    randomizor = Random.Range(3, 8);
                }
                else if (fruitwant == 2)
                {
                    randomizor = Random.Range(3, 6);
                }
                else if (fruitwant == 3)
                {
                    randomizor = Random.Range(2, 4);
                }
                else if (fruitwant == 4)
                {
                    randomizor = Random.Range(1, 3);
                }
                manage.playsound("Eat");
            }
        }
        else if (fruitwant == 2)
        {
            if (currentgrape >= randomizor)
            {
                currentgrape -= randomizor;
                ragebar.SetHealth(currentRage = randomizor * 6);

                fruitwant = Random.Range(1, 3);
                if (fruitwant == 1)
                {
                    randomizor = Random.Range(4, 8);
                }
                else if (fruitwant == 2)
                {
                    randomizor = Random.Range(3, 6);
                }
                else if (fruitwant == 3)
                {
                    randomizor = Random.Range(2, 4);
                }
                else if (fruitwant == 4)
                {
                    randomizor = Random.Range(1, 2);
                }
                manage.playsound("Eat");
            }
        }
        /*else if (fruitwant == 3)
        {
            if (ngor.currentmelon >= randomizor)
            {
                ngor.currentmelon -= randomizor;
                ragebar.SetHealth(ngor.currentRage = ngor.maxRage);

                fruitwant = Random.Range(1, 5);
                if (fruitwant == 1)
                {
                    randomizor = Random.Range(4, 8);
                }
                else if (fruitwant == 2)
                {
                    randomizor = Random.Range(3, 6);
                }
                else if (fruitwant == 3)
                {
                    randomizor = Random.Range(2, 4);
                }
                else if (fruitwant == 4)
                {
                    randomizor = Random.Range(1, 2);
                }
            }
        }
        else if (fruitwant == 4)
        {
            if (ngor.currentdragon >= randomizor)
            {
                ngor.currentdragon -= randomizor;
                ragebar.SetHealth(ngor.currentRage = ngor.maxRage);

                fruitwant = Random.Range(1, 5);
                if (fruitwant == 1)
                {
                    randomizor = Random.Range(4, 8);
                }
                else if (fruitwant == 2)
                {
                    randomizor = Random.Range(3, 6);
                }
                else if (fruitwant == 3)
                {
                    randomizor = Random.Range(2, 4);
                }
                else if (fruitwant == 4)
                {
                    randomizor = Random.Range(1, 2);
                }
            }
        }*/
    }


    void Start()
    {
        currentRage = maxRage;
        ragebar.SetMaxHealth(maxRage);
        
        fruitwant = 1;
        randomizor = 3;
        //   foodformat = foodamount.text;

    }

    // Update is called once per frame
    void Update()
    {
        UIUpdater();
        if (currentRage <= 0)
        {
            gameover.SetActive(true);
            redeye.SetActive(true);
            if (loop)
            {
                loop = !loop;
                manage.playsound("RedEye");
            }
        }
        if (currentRage > maxRage)
        {
            currentRage = maxRage;
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            currentapple = 100;
            currentgrape = 100;
            currentmelon = 100;
            currentdragon = 100;
        }

        if (fruitwant == 1)
        {
            apple.enabled = true;
        }
        else
        {
            apple.enabled = false;
        }
        if (fruitwant == 2)
        {
            grape.enabled = true;
        }
        else
        {
            grape.enabled = false;
        }
        if (fruitwant == 3)
        {
            melon.enabled = true;
        }
        else
        {
            melon.enabled = false;
        }
        if (fruitwant == 4)
        {
            dragon.enabled = true;
        }
        else
        {
            dragon.enabled = false;
        }
    }
}
