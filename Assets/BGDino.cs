﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGDino : MonoBehaviour
{
    private void Update()
    {
        transform.Translate(Vector2.right * 3.5f * Time.deltaTime);
    }
    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.CompareTag("Counter"))
        {
            Destroy(gameObject);
        }
    }
}
